window.debounce = function (func, wait, immediate) {
	var timeout, result;
	return function () {
		var context = this,
			args = arguments;
		var later = function () {
			timeout = null;
			if (!immediate) result = func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) result = func.apply(context, args);
		return result;
	};
};

window.aceEditor = function (id, type) {
	type = type || 'html';
	var conf = {
		editor: ace.edit(id),
		init: function () {
			var editor = this.editor;

			editor.setOptions({
				// theme: 'ace/theme/twilight',
				theme: 'ace/theme/monokai',
				showPrintMargin: false,
				//printMarginColumn: 140,
				tabSize: 2,
				minLines: 20,
				maxLines: 10000,
				autoScrollEditorIntoView: true,
				enableBasicAutocompletion: true,
				enableSnippets: true,
				enableLiveAutocompletion: false,
				useSoftTabs: true,
				highlightActiveLine: false
			});

			editor.getSession().setMode('ace/mode/' + type);

			editor.$blockScrolling = Infinity;

			if (type === 'html' || type === 'css') {//添加emmet插件的支持
				editor.setOption('enableEmmet', true);
			}

		},
		val: function (v) {
			if (v) {
				return this.editor.setValue(v);
			}
			return $.trim(this.editor.getValue());
		}
	};
	conf.init();
	return conf;
};

(function (global, d, $) {
	var controls = {
		box: d.getElementById('controls-box')
	};

	/**
	 * 文本输入框(单行)
	 * input|text
	 */
	var InputTextSingle = React.createClass({
		getInitialState: function () {
			return {
				text: '文本输入框(单行)'
			}
		},
		handleChange: function (event) {
			this.setState({text: event.target.value});
		},
		render: function () {
			var _d = this.props.data;
			return (
				<div className="ctrl-row">
					<label className="title" htmlFor={_d.name}>场景名称</label>

					<div className="box">
						<div className="ind-input">
							<input type="text"
							       placeholder={_d.placeholder}
							       value={this.state.text}
							       name={_d.name}
							       onChange={this.handleChange} />
						</div>
					</div>
				</div>
			);
		}
	});

	/**
	 * 文本输入框(多行)
	 * input|text
	 */
	var InputTextMulti = React.createClass({
		getInitialState: function () {
			return {
				text: '测试多行文本控件'
			}
		},
		handleChange: function (event) {
			this.setState({text: event.target.value});
		},
		render: function () {
			var _d = this.props.data;
			return (
				<div className="multi-row">
					<label className="title" htmlFor={_d.name}>场景名称</label>

					<div className="box">
						<div className="ind-input">
							<input type="text"
							       placeholder={_d.placeholder}
							       value={this.state.text}
							       name={_d.name}
							       onChange={this.handleChange} />
						</div>
					</div>
				</div>
			);
		}
	});

	/* 上传组件 */
	var InputUpload = React.createClass({
		getInitialState: function () {
			return {
				value: this.props.defaultValue
			};
		},
		handleChange: function (event) {
			var _this = this;
			var curVal = event.target.value;
			_this.setValue(curVal, function () {
				// _this.props.parent.handleChange();
			});
		},
		getValue: function () {
			return this.state.value;
		},
		setValue: function (val, fn) {
			this.setState({value: val}, fn);
		},
		getUploadFile: function () {
			var _this = this;
			// Actions.openUploadWindow(_this.props.data.range, function (path) {
			// 	_this.setState({value: path}, function () {
			// 		_this.props.parent.handleChange(_this.props.data.propsName, 'image');
			// 	});
			// });

			console.log('call upload window');

		},
		render: function () {
			return (
				<div className="ctrl-row">
					<label htmlFor={this.props.data.title} className="title">{this.props.data.title}</label>
					<div className="box">
						<div className="ind-upload">
							<input name={this.props.data.title}
							       type="text"
							       ref="picUrl"
							       onChange={this.handleChange}
							       value={this.state.value} />
							<button className="ind-btn" type="button" onClick={this.getUploadFile}>获取文件</button>
						</div>
					</div>
				</div>
			)
		}
	});

	/**
	 * 颜色选择器
	 */
	var InputColor = React.createClass({
		getInitialState: function () {
			return {
				color: '#000000'
			}
		},
		handleChange: function (event) {
			this.setState({color: event.target.value});
		},
		render: function () {
			var _v = this.state.color,
				_d = this.props.data;

			return (
				<div className="ctrl-row">
					<label htmlFor={_d.colorName} className="title">背景颜色</label>
					<div className="box">
						<div className="ind-input mod-settings-dib-input__color">
							<input type="color"
							       name={_d.colorName}
							       className="colorPicker colorpicker-element"
							       placeholder={_d.placeholder}
							       value={_v}
							       onChange={this.handleChange} />
						</div>
						<div className="ind-input mod-settings-dib-input">
							<input type="text"
							       className="colorPicker"
							       name={_d.textName}
							       placeholder={_d.placeholder}
							       value={_v}
							       onChange={this.handleChange} />
						</div>
					</div>
				</div>
			);
		}
	});

	/**
	 * 滑块 | 依赖jqueryui + peslider
	 */
	var InputSlider = React.createClass({
		getInitialState: function () {
			return {
				value: this.props.data.value,
				min: this.props.data.min,
				max: this.props.data.max,
				step: this.props.data.step
			}
		},
		handleChange: function (e) {
			this.setState({
				value: e.target.value
			});

			console.log(this.state.value);
		},
		componentDidMount: function () {
			var _t = this,
				_ipt = $(_t.refs.ipt),
				_config = {
					range: 'min',
					value: _t.state.value,
					min: _t.props.data.min,
					max: _t.props.data.max,
					step: _t.state.step,
					callback: global.debounce(function (e) {
						console.log(11111);
						_t.handleChange(e);
					}, 500)
				};

			_ipt.peSlider(_config);
		},
		render: function () {

			return (
				<div className="ctrl-row">
					<label htmlFor="" className="title">宽度</label>
					<div className="box">
						<div className="ind-slider">

							<input className="slider-ipt"
							       type="number"
							       name="itemW"
							       id="itemW"
							       min={this.state.min}
							       max={this.state.max}
							       step={this.state.step}
							       value={this.state.value}
							       onChange={this.handleChange}
							       data-preview={this.state.value}
							       ref="ipt" />
						</div>
					</div>
				</div>
			);
		}
	});

	/**
	 * 复选框
	 */
	var InputCheckBox = React.createClass({
		getInitialState: function () {
			var _value = {
				timestamp: new Date().getTime()
			};

			this.props.data.map(function (n, i) {
				_value[n.name] = n.checked;
			});

			return _value;
		},
		handleChange: function (e) {
			var _name = e.target.name,
				_checked = e.target.checked;

			this.setState(function (n) {
				n[_name] = _checked;
			});

		},
		render: function () {
			var _t = this;

			var Nodes = this.props.data.map(function (n, i) {
				return (
					<div className="ind-checkbox" key={i}>
						<input type="checkbox"
						       name={n.name}
						       id={n.name+'_'+_t.state.timestamp}
						       checked={_t.state[n.name]}
						       onChange={_t.handleChange} />
						<label htmlFor={n.name+'_'+_t.state.timestamp} className="ind-checkbox_txt">{n.txt}</label>
					</div>
				);

			});

			return (
				<div className="box">
					{Nodes}
				</div>
			);
		}
	});

	/* 复选框组 */
	var InputCheckBoxGroup = React.createClass({

		render: function () {
			return (
				<div className="ctrl-row">
					<div className="title">{this.props.data.name}</div>

					<InputCheckBox data={this.props.data.item} />

				</div>
			);
		}
	});

	//简单textarea
	var TextareaSimple = React.createClass({
		getInitialState: function () {
			return {
				value: this.props.defaultValue
			};
		},
		handleChange: function (event) {
			var _this = this;
			var curVal = event.target.value;
			_this.setValue(curVal, function () {
				// _this.props.parent.handleChange();
				console.log('change');
			});

		},
		getValue: function () {
			return this.state.value;
		},
		setValue: function (val, fn) {
			this.setState({value: val}, fn);
		},

		render: function () {
			var _d = this.props.data;
			return (
				<div className="multi-row">
					<label className="title"
					       htmlFor={_d.propsName}>{_d.title}</label>

					<div className="box">
						<div className="ind-textarea">
							<textarea name={_d.propsName} onChange={this.handleChange} rows="5" value={this.state.value} />
						</div>
					</div>
				</div>
			);
		}
	});

	//富文本编辑器
	var TextareaRich = React.createClass({
		getInitialState: function () {
			return {
				value: this.props.defaultValue
			};
		},
		handleChange: function (event) {
			var _this = this;
			var curVal = event.target.value;
			_this.setValue(curVal, function () {
				// _this.props.parent.handleChange();
				console.log('change');
			});

		},
		getValue: function () {
			return this.state.value;
		},
		setValue: function (val, fn) {
			this.setState({value: val}, fn);
		},

		componentDidMount: function () {
			var _this = this,
				_d = _this.props.data;

			var ue = UE.getEditor('ind-rich-box');

			ue.ready(function () {
				ue.setContent(_d.defaultValue);

				ue.addListener('contentChange', function (e) {
					_this.setValue(ue.getContent(), function () {
						console.log('change');
					});
				})
			});

		},

		render: function () {
			var _d = this.props.data;
			return (
				<div className="multi-row">
					<label className="title"
					       htmlFor={_d.propsName}>{_d.title}</label>

					<div className="box">
						<div className="ind-rich">
							<div className="ind-rich-box" id="ind-rich-box"></div>
						</div>
					</div>
				</div>
			);
		}
	});

	//代码编辑器
	var TextareaCode = React.createClass({
		getInitialState: function () {
			return {
				value: this.props.defaultValue
			};
		},
		handleChange: function (event) {
			var _this = this;
			var curVal = event.target.value;
			_this.setValue(curVal, function () {
				// _this.props.parent.handleChange();
				console.log('change');
			});

		},
		getValue: function () {
			return this.state.value;
		},
		setValue: function (val, fn) {
			this.setState({value: val}, fn);
		},

		componentDidMount: function () {
			var _this = this,
				_d = _this.props.data;

			// global.aceEditor('ind-code-box', 'javascript');
			var _e = global.aceEditor(_d.id, _d.type);

			_e.val(_d.defaultValue);

			$('#' + _d.btn).on('click', function () {

				_this.setValue(_e.val(), function () {
					console.log(_e.val());
				});
			});

		},

		render: function () {
			var _d = this.props.data;
			return (
				<div className="multi-row">
					<label className="title"
					       htmlFor={_d.propsName}>{_d.title}</label>

					<div className="box">
						<div className="ind-code">
							<div className="ind-code-box" id={_d.id}></div>
						</div>

						<button className="btn btn-primary" id={_d.btn}>保存</button>
					</div>
				</div>
			);
		}
	});

	var _data = {
		text: {
			name: 'appConfigName',
			placeholder: '输入您要的文本'
		},

		textarea: {
			propsName: 'textarea',
			title: '文本域'
		},

		rich: {
			propsName: 'rich',
			title: '富文本编辑器',
			defaultValue: '<p>新追加的内容</p>'
		},

		code_js: {
			propsName: 'code',
			title: 'js代码编辑器',
			type: 'javascript',
			id: 'code_js',
			btn: 'code_js_btn',
			defaultValue: 'function(){\n  console.log(123)\n}'
		},

		code_css: {
			propsName: 'code',
			title: 'css代码编辑器',
			type: 'css',
			id: 'code_css',
			btn: 'code_css_btn',
			defaultValue: '.test{\n  font-size:100px;\n  line-height: 1;\n}'
		},

		color: {
			colorName: 'itemBackgroundColor',
			placeholder: '点击选择',
			textName: 'itemBackgroundColorText'
		},
		slider: {
			value: 100,
			min: 0,
			max: 320,
			step: 10
		},
		checkbox: {
			name: '测试复选框',
			item: [
				{
					txt: '选项A',
					name: 'checkboxA',
					checked: false
				},
				{
					txt: '选项B',
					name: 'checkboxB',
					checked: true
				},
				{
					txt: '选项C',
					name: 'checkboxC',
					checked: true
				}
			]
		}
	};

	var DomList = React.createClass({
		render: function () {
			return (
				<div className="app-config-item_bd" style={{'display':'block'}}>
					<InputTextSingle data={this.props.data.text} />
					<InputUpload data={this.props.data.text} />
					<InputTextMulti data={this.props.data.text} />
					<TextareaSimple data={this.props.data.textarea} />
					<TextareaRich data={this.props.data.rich} />
					<TextareaCode data={this.props.data.code_js} />
					<TextareaCode data={this.props.data.code_css} />
					<InputSlider data={this.props.data.slider} />
					<InputSlider data={
						{
							value: 300,
							min: 0,
							max: 640,
							step: 0.1
						}
					} />
					<InputColor data={this.props.data.color} />
					<InputCheckBoxGroup data={this.props.data.checkbox} />
				</div>
			);
		}
	});

	ReactDOM.render(
		<DomList data={_data} />,
		controls.box
	);

	global.controls = controls;

})(window, document, jQuery);



