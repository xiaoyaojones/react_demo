$.fn.peSlider = function (settings) {

	//configurable options (none so far)
	var o = $.extend({}, settings);

	return $(this).each(function () {
		var _t = $(this);
		var slider = $('<div></div>');

		if (_t.is('input')) {

			var input = $(this);
			var sliderOptions = $.extend(o, {
				min: parseFloat(settings.min),
				max: parseFloat(settings.max),
				value: parseFloat(settings.value)
			});

			slider
				.insertBefore(input)
				.slider(sliderOptions)
				.bind('slide', function (e, ui) {
					input.val(ui.value);

					input.trigger("change");
				});

			input
				.keyup(function () {
					var inVal = parseFloat(input.val());
					if (!isNaN(inVal)) {
						slider.slider('value', inVal);
						input.val(slider.slider('value'));
					}
				})
				.blur(function () {
					var inVal = parseFloat(input.val());
					if (isNaN(inVal)) {
						input.val(0);
					}
				})
				.change(function (e) {
					input.attr("data-preview", parseFloat(input.val()));

					var inVal = parseFloat(input.val());
					if (!isNaN(inVal)) {
						slider.slider('value', inVal);
					}

					if (settings.callback) {
						settings.callback(e);
					}
				});

			if (!settings.step) {
				var step = Math.round(parseFloat(settings.max) / slider.width());
				if (step > 1) {
					slider.slider('option', 'step', step);
				}
			}

		}
	});
};



