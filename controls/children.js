(function (global){
	
	
	
	var Children = window.Widget.InputTextSingle;
	
	var _data = {
		name: 'test children',
		children: {
			name: 'title',
			prop: 'child',
			value: 'hahahaha'
		}
	};

	var Parent = React.createClass({
		getInitialState: function (){
			return _data
		},
		handleChange: function (v){
			
		},
		saveData: function (v) {
			console.log(v);
			this.setState({
				children: {
					value: v
				}
			});
		},
		render: function () {
			return (
				<div>
					<Children data={this.props.data} parent={this} />
					{this.state.children.value}
				</div>
			);
		}
	});
	
	ReactDOM.render(
		<Parent data={_data} />,
		document.getElementById('controls-box')
	);
	
})(window);