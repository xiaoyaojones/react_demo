(function (global){
	global.Widget = {};

	global.Widget.InputTextSingle = React.createClass({
		getInitialState: function () {
			return {
				value: this.props.data.children.value
			}
		},
		handleChange: function (event) {
			console.log(event.target.value);
			this.setState({value: event.target.value});
			console.log(this.state.value);
			this.props.parent.saveData(this.state.value);
			console.log('=====end======');
		},
		// componentWillUpdate: function (){
		//
		// },
		render: function () {
			var _d = this.props.data.children;
			return (
				<div className="ctrl-row">
					<label className="title" htmlFor={_d.prop}>{_d.name}</label>

					<div className="box">
						<div className="ind-input">
							<input type="text"
							       value={this.state.value}
							       name={_d.prop}
							       onChange={this.handleChange} />
						</div>
					</div>
				</div>
			);
		}
	});

})(window);



