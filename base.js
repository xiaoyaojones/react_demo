var data = [
	{
		'id': 1,
		'author': 'jones',
		'text': 'hello world! I\'m jones'
	},
	{
		'id': 2,
		'author': 'jones1',
		'text': 'hello, I\'m jones1'
	}
];

var Comment = React.createClass({

	render: function (){
		return (
			<div className="comment">
				<h2 className="comment-author">{this.props.author}</h2>
				<p>{this.props.children}</p>
			</div>
		);
	}
});

var CommentForm = React.createClass({
	render: function () {
		return (
			<div className="comment-form">
				hello, I'm comment-form
			</div>
		);
	}
});

var CommentList = React.createClass({
	render: function () {
		var _item = this.props.data.map(function (comment){
			return (
				<div>
					<Comment author={comment.author} key={comment.id}>
						{comment.text}
					</Comment>
					<CommentForm />
				</div>

			);
		});

		return (
			<div className="comment-list">
				{_item}
			</div>
		);
	}
});


var CommentBox = React.createClass({
	render: function () {
		return (
			<div className="comment-box">
				<h1>Title</h1>
				<CommentList data={this.props.data} />
			</div>
		);
	}
});

ReactDOM.render(
	<CommentBox data={data} />,
	document.getElementById('base')
);

